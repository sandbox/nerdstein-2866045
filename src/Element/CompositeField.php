<?php

namespace Drupal\composite_field\Element;

use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a composite field form element.
 *
 * Usage example:
 * @code
 * $form['my_field'] = [
 *   '#type' => 'composite'
 * ];
 * @endcode
 *
 * @FormElement("composite")
 */
class CompositeField extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#multiple' => TRUE,
      '#default_value' => NULL,
      '#theme_wrappers' => ['container'],
    ];
  }
}